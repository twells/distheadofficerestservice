<?php 
	
$islocal = isset($_GET['islocal']) ? $_GET['islocal'] : 0;
$isexternal = isset($_GET['isexternal']) ? $_GET['isexternal'] : 0;
if ($islocal == 0 && $isexternal == 0)
	$islocal = 1;
	
define('IS_LOCAL', $islocal);
define('IS_EXTERNAL', $isexternal);
define('RESTSERVICESROOT_TAG', 'restservicesroot'); // used in examples and should be replaced with the actual rest ervice home path when outputting.

if (IS_LOCAL) {
	define('REST_SERVICE_HOME', 'distheadofficelocalrestservice');
	define('REST_SERVICE_TITLE', 'Distributors Head Office Local REST Web Services');		
}
else if (IS_EXTERNAL) {
	define('REST_SERVICE_HOME', 'distheadofficerestservice');
	define('REST_SERVICE_TITLE', 'Distributors Head Office REST Web Services');
}	

if (!function_exists("json_encode")) {
	require_once "inc/utils/simplejson.php";
	function json_encode($value) {
		return toJSON($value);
	}
	
	function json_decode( $json, $assoc = false ) {
		return fromJSON($json, $assoc);
	}	
	
}

if (!defined('CLINPUTFILE_REPLACETAG'))
	define('CLINPUTFILE_REPLACETAG','__CLINPUTFILE__');

if (!defined('DISTHO_REST_SERVICE_LOG')) {
	
	$t = time(); // log path is relative to this time
	
	define('DISTHO_REST_SERVICE_LOG_ROOT', 'logs/'); // root directory where logs are
	
	$log_yyyy_dir = date('Y', $t) . "/";
	$log_mm_dir = date('m', $t) . "/";
	
	// check that the year directory exists, if not, let's create it
	$current_log_path = DISTHO_REST_SERVICE_LOG_ROOT . $log_yyyy_dir;
	if (!file_exists($current_log_path))
		mkdir($current_log_path);

	// check that the month directory exists, if not, let's create it	
	$current_log_path .= $log_mm_dir;
	if (!file_exists($current_log_path))
		mkdir($current_log_path);
	
	// now define our constant to where the log file should be
	define('DISTHO_REST_SERVICE_LOG', $current_log_path . REST_SERVICE_HOME . '-' . date('Ymd', $t) . '.log');
}

if (!defined('DISTHO_HOST')) {
	$localIP = getHostByName(php_uname('n'));
	define('DISTHO_HOST', "http://{$localIP}/" . REST_SERVICE_HOME . "/");
	//echo DISTHO_HOST;
}

function GenerateResponse($Status, $Message = '', $Data = null, $RequestId = '') {
	$Response = array();	
	if ($RequestId != '')
		$Response['RequestId'] = $RequestId;
	$Response['Status'] = $Status;
	$Response['Message'] = $Message;
	if (!is_null($Data)) {
		$Response['Data'] = $Data;
	}	
	return $Response;
}

function LogResponse($Response) {
	$logdata = date('YmdHis') . " {$_SERVER["REQUEST_METHOD"]} Response " . basename($_SERVER['REQUEST_URI']) . " ";
	$logdata .= json_encode($Response) . PHP_EOL;
	file_put_contents(DISTHO_REST_SERVICE_LOG, $logdata, FILE_APPEND);	
}

function OutputResponseArrayAndExit($Response) {	
	$protocol = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0';
	header($protocol.' '. 200 .' Complete');	
	echo json_encode($Response);
	exit;
}

function OutputResponseAndExit($Status, $Message = '', $Data = null) {
	$Response = GenerateResponse($Status, $Message, $Data);	
	$protocol = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0';
	header($protocol.' '. $Status .' '. $Message);	
	echo json_encode($Response);
	exit;
}

function MyDecodeJson($json, &$DecodedArray, &$err) {
	$err = '';
	$DecodedArray = json_decode($json, true);
	
	if (!is_null($DecodedArray))
		return true;
	
	switch (json_last_error()) {
		case JSON_ERROR_NONE:
			$err = ' - No errors';
		break;
		case JSON_ERROR_DEPTH:
			$err = ' - Maximum stack depth exceeded';
		break;
		case JSON_ERROR_STATE_MISMATCH:
			$err = ' - Underflow or the modes mismatch';
		break;
		case JSON_ERROR_CTRL_CHAR:
			$err = ' - Unexpected control character found';
		break;
		case JSON_ERROR_SYNTAX:
			$err = ' - Syntax error, malformed JSON';
		break;
		case JSON_ERROR_UTF8:
			$err = ' - Malformed UTF-8 characters, possibly incorrectly encoded';
		break;
		default:
			$err = ' - Unknown error';
		break;
	}		
	
	return false;
}

function GetHomePath() {
	return '';
}

function GetActionRequest($RequestMethod) {
	$http_raw_data = '';
	
	$logdata = date('YmdHis') . " {$_SERVER["REQUEST_METHOD"]} Request " . basename($_SERVER['REQUEST_URI']);		
		
	
	if (isset($_SERVER["CONTENT_TYPE"]) && $_SERVER["CONTENT_TYPE"] == "application/json") {
		$http_raw_data = file_get_contents("php://input");				
		file_put_contents(DISTHO_REST_SERVICE_LOG, $logdata . " {$http_raw_data}" . PHP_EOL, FILE_APPEND);	
	}
	else {
		file_put_contents(DISTHO_REST_SERVICE_LOG, $logdata . PHP_EOL, FILE_APPEND);
		OutputResponseAndExit(400, "Content Type of 'application/json' is required");
	} 		
	
	if (isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == $RequestMethod) {
		// do nothing
	}
	else if (isset($_SERVER["REQUEST_METHOD"])) {
		OutputResponseAndExit(405, "Wrong Request Type. {$_SERVER["REQUEST_METHOD"]} was supplied but {$RequestMethod} was expected.");
	}
	else {
		OutputResponseAndExit(400, "No Request Type supplied  but {$RequestMethod} was expected.");
	}
	
	return $http_raw_data;
}

function SendCurlJsonRequest($url, $request_type, $json) {

	$ch = curl_init($url);

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request_type);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Content-Type: application/json',
	'Content-Length: ' . strlen($json))
	);

	return curl_exec($ch);		
}

function GetSiteHost() {
	return $_SERVER['HTTP_HOST']; 
}

function GetSiteProtocol() {
	if (isset($_SERVER['HTTPS']))
		$protocol = 'https';
	else
		$protocol = 'http';	
	return $protocol;
}

function GetBaseURL() {
	$protocol = GetSiteProtocol();
	$httphost = GetSiteHost();	
	
	return "{$protocol}://{$httphost}/" . REST_SERVICE_HOME;
}

function TopNavigation($Title = '') {
	
	require_once 'modules/TopMenu/TopMenu.controller.php';

	$controller = new TopMenuController();
	$controller->OutputSiteHeader($Title);
	
}

function SiteHeader($custom_head_html = '') {
	$base = GetBaseURL() . "/";
	
	$title = REST_SERVICE_TITLE;
	
	echo "
			
<!DOCTYPE html>
<html class=\" js no-touch svg inlinesvg svgclippaths no-ie8compat\" lang=\"en\" style=\"\"><!--<![endif]--><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
  <meta charset=\"utf-8\">

  <!-- Set the viewport width to device width for mobile -->
  <meta name=\"viewport\" content=\"width=device-width\">
  
  
  <base href='{$base}' target='_self'>

  <title>{$title}</title>
  
  
  <link href='img/favicon.ico' rel='shortcut icon' type='image/vnd.microsoft.icon'>
  
  <link rel='apple-touch-icon' href='favicon.ico'>
  
  <link rel='stylesheet' href='css/normalize.css'>
  <link rel='stylesheet' href='css/foundation.css'>
  <link href='css/font-awesome.css' rel='stylesheet'>
  <link href='css/font-awesome-ie7.css' rel='stylesheet'>  
  

	<link rel='stylesheet' type='text/css' href='css/topbar.css' />



  
  <link rel='stylesheet' type='text/css' href='css/print.css' />
  

  <script src='js/custom.modernizr.js'></script>
  		
  <script type='text/javascript' class='jsbin' src='js/jquery-1.10.2.min.js'></script>
  
  <link href='css/font-awesome.css' rel='stylesheet'>
<link href='css/font-awesome-ie7.css' rel='stylesheet'>
<!-- foundation datepicker start -->
<script src='js/foundation-datepicker.js'></script>
<link rel='stylesheet' href='css/foundation-datepicker.css'>
<!-- foundation datepicker end -->
  
  
  {$custom_head_html}
  

</head>
<body style='padding:10px; '>			
			
			";	
}

function SiteFooter() {
	
	echo '
	<!-- Footer -->
	
	<footer class="row" style="max-width: 150em;">
	<div class="large-12 columns">
			<hr>
      <div class="row">
	    <div class="large-1 columns">&nbsp;</div>
        <div class="large-5 columns">
          <p>&nbsp;</p>
        </div>
        <div class="large-5 columns">
          <ul class="inline-list right">
            <li><a href="http://www.acr.com.au">ACR Business Systems</a></li>
			<!--li><a href="#">Privacy Policy</a></li-->
          </ul>
        </div>
		<div class="large-1 columns"></div>
      </div>
	      </footer>
	
	      <!-- script>
	      document.write("<script src=js/vendor/" +
	      		("__proto__" in {} ? "zepto" : "jquery") +
  ".js><\/script>")
	  </script-->
	  <script src="js/zepto.js"></script>
	  <script src="js/foundation.min.js"></script>
  <script>
    $(document).foundation();
	    </script>
	
	
	
	    <!-- End Footer -->
	
	
	    </body></html>';	
}


function prettyPrint( $json )
{
    $result = '';
    $level = 0;
    $in_quotes = false;
    $in_escape = false;
    $ends_line_level = NULL;
    $json_length = strlen( $json );

    for( $i = 0; $i < $json_length; $i++ ) {
        $char = $json[$i];
        $new_line_level = NULL;
        $post = "";
        if( $ends_line_level !== NULL ) {
            $new_line_level = $ends_line_level;
            $ends_line_level = NULL;
        }
        if ( $in_escape ) {
            $in_escape = false;
        } else if( $char === '"' ) {
            $in_quotes = !$in_quotes;
        } else if( ! $in_quotes ) {
            switch( $char ) {
                case '}': case ']':
                    $level--;
                    $ends_line_level = NULL;
                    $new_line_level = $level;
                    break;

                case '{': case '[':
                    $level++;
                case ',':
                    $ends_line_level = $level;
                    break;

                case ':':
                    $post = " ";
                    break;

                case " ": case "\t": case "\n": case "\r":
                    $char = "";
                    $ends_line_level = $new_line_level;
                    $new_line_level = NULL;
                    break;
            }
        } else if ( $char === '\\' ) {
            $in_escape = true;
        }
        if( $new_line_level !== NULL ) {
            $result .= "\n".str_repeat( "\t", $new_line_level );
        }
        $result .= $char.$post;
    }

    return $result;
}

?>