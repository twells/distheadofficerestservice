<?php 


//require_once('inc/site_lib.php');
require_once 'modules/TopMenu/MenuItem.class.php';
//require_once 'inc/SecurityUtils.php';

// this class is currently quite simple but in due course it
// should load menus from DB and also take into acccount the site or user.


class TopMenuModel {

	protected $menus;
	
	function __construct() {
		$menus = array();		
	}
	
	function FetchMenus() {
		// hard code for now.		
		
		$menu = new MenuItem('Service Usage', '');
			
		$target_dir = "modules/";

		if (!file_exists($target_dir)) 
			OutputResponseAndExit(404, 'The requested page does not exist.');
	
		
		foreach(glob("{$target_dir}*") as $path) {
			$module = basename($path);
			
			$skip = true;
			
			$access_file = "{$path}/{$module}.access.json";
			if (file_exists($access_file)) {
				$access_json = file_get_contents($access_file);
				//echo $access_json . "<br>";
				$module_acess = json_decode($access_json, true);
				//echo "[L" . IS_LOCAL . "]<br>";
				//echo "[E" . IS_EXTERNAL . "]<br>";
				if (!is_null($module_acess) && isset($module_acess['isenabled']) && $module_acess['isenabled'] == 1) {
					if (IS_LOCAL && isset($module_acess['islocal']) && $module_acess['islocal'] == 1) 
						$skip = false;
					else if (IS_EXTERNAL && isset($module_acess['isexternal']) && $module_acess['isexternal'] == 1)
						$skip = false; 
				}
			}
			
			if ($skip)
				continue;
			
			
			$usage_file = "{$path}/{$module}.usage.json";
			
			if (file_exists($usage_file)) {
				$menu->AddMenuItem(new MenuItem($module, "{$module}/Usage"));
			}
		}	

		$this->menus['Service Usage'] = $menu;




		
		
		return $this->menus;
		
	}
	

}

?>