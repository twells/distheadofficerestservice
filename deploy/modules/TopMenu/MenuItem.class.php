<?php 

class MenuItem {

	protected $label;
	protected $url;
	protected $children;
	protected $ul_class;
	
	
	function __construct($label, $url = '', $ul_class = 'left') {
		$this->label = $label;
		$this->url = $url;
		$this->children = array();
		$this->ul_class = $ul_class;
	}
	
	function AddMenuItem(MenuItem $item) {
		$this->children[] = $item; 
	}

	function Output() {
		if ($this->url == '')
			$onclick = 'onclick="return false;"';
		else
			$onclick = '';
			
		$has_dropdown = count($this->children) > 0 ? 'has-dropdown' : '';
		
		echo "
    <section class='top-bar-section' style='background:#CA1622;'>
      <!-- Right Nav Section -->
      <ul class='{$this->ul_class}'>
		
        <li class='{$has_dropdown}'>
          <a href='{$this->url}' {$onclick}>{$this->label}</a>
          <ul class='dropdown'>";
		
		foreach ($this->children as $child)
			echo "<li><a href='{$child->url}'>{$child->label}</a></li>";
         
         echo   
          "</ul>
        </li>
      </ul>
		
		
    </section>
			";		
	}
	
	function AsTable() {
		echo "<table>";
		foreach ($this->children as $child) {
			echo "<tr><td><a href='{$child->url}'>{$child->label}</a></td></tr>";
		}
		echo "</table>";
	}
	
}

?>