<?php 


//require_once('inc/site_lib.php');
require_once("modules/TopMenu/TopMenu.model.php");
require_once("modules/TopMenu/TopMenu.view.php");

class TopMenuController {

	private $model;
	private $view;
	
	function __construct(TopMenuModel &$model = null, TopMenuView &$view = null) {
		if (!$model)
			$model = new TopMenuModel();
		
		if (!$view)
			$view = new TopMenuView();
		
		$this->model = $model;
		$this->view = $view;
	}
	
	function OutputSiteHeader($Title = '') {
		$this->view->SetMenus($this->model->FetchMenus());
		$this->view->OutputSiteHeader($Title);
	}
	
	function OutputMenuAsTable($MenuItemLabel) {
		$this->view->SetMenus($this->model->FetchMenus());
		$this->view->OutputMenuItemAsTable($MenuItemLabel);
	}

}

?>