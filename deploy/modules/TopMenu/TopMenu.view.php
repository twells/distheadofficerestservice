<?php 


require_once('inc/utils/site_lib.php');


class TopMenuView {

	protected $menus;
	
	function __construct() {

	}
	
	function SetMenus($menus) {
		$this->menus = $menus;
	}
	
	 
	function OutputSiteHeader($Title = '') {
		$home_url = GetHomePath();
		
		
		
		echo '
    	
	        				
  <nav class="top-bar" >
    <ul class="title-area">
				
      <li class="name">
        <h1>
		    <a href="' . $home_url . '" >
	          	Home&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
	        </a>
        </h1>
      </li>				
				
      <li class="toggle-topbar menu-icon" ><a href="#"><span>menu</span></a></li>
    </ul>';
		
		if (count($this->menus) > 0) {
			foreach ($this->menus as $menu) 
				echo $menu->Output();
		}
				
		echo '
	    		
	    		
  </nav>
  <!-- End Nav -->
				

  <div class="row" style="max-width: 150em;">
    <div class="small-3 columns">
      <img src="img/ACR-Logo.png">	
    </div>
    <div class="small-9 columns">
	  <h2>' . $Title . '</h2>			
    </div>			
    <div class="small-12 columns">
      <hr>
    </div>
    
  </div>
				
				
';		
	}
	
	function OutputMenuItemAsTable($MenuItemName) {
		foreach ($this->menus as $key => $menu)
			if ($key == $MenuItemName) {
				echo $menu->AsTable();
			}				
	}	

}

?>