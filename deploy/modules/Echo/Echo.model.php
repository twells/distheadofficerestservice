<?php 





class EchoModel extends AbstractModel {

	public $echoMessage = "Silence is golden!"; // default message

	function __construct() {		
		//self::$echoMessage = "";
		
		$this->testUpsertJson['Test1'] = array('method'=> 'HelloWorldOn-' . date('Ymd-His'), 'Description' => 'Echo test from browser request', 'json' => null);
		$this->testUpsertJson['Test2'] = array('method'=> 'PUT', 'Description' => 'Echo test of PUT from web request', 'json' => '{"Message":"This data was sent via a PUT request at ' . date('Y-m-d H:i:s') . '"}');
		$this->testUpsertJson['Test3'] = array('method'=> 'GET', 'Description' => 'Echo test of GET from web request', 'json' => '{"Message":"This data was sent via a GET request at ' . date('Y-m-d H:i:s') . '"}');
		$this->testUpsertJson['Test4'] = array('method'=> 'DELETE', 'Description' => 'Echo test of DELETE from web request', 'json' => '{"Message":"This data was sent via a DELETE request at ' . date('Y-m-d H:i:s') . '"}');
		$this->testUpsertJson['Test5'] = array('method'=> 'POST', 'Description' => 'Echo test of POST from web request', 'json' => '{"Message":"This data was sent via a POST request at ' . date('Y-m-d H:i:s') . '"}');
		
	}

	function SetEchoMessage($Message) {
		if ('phpinfo' == $Message)
			$this->echoMessage = phpinfo();
		else
			$this->echoMessage = $Message;
	}
	
}
	
?>