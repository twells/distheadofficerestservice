<?php 



require_once("modules/Echo/Echo.model.php");
require_once("modules/Echo/Echo.view.php");

	
	
	

class EchoController extends AbstractController {

	
	
	function __construct() {
		self::$ModuleName = "Echo";
		parent::__construct();
		
	}

	function EchoTest($resource) {
		echo "Test Id: {$resource}<br>";
		if (isset($this->model->testUpsertJson[$resource])) {
			$TestArray = $this->model->testUpsertJson[$resource];

			$url = DISTHO_HOST . "Echo/View/" . $TestArray['method'];
			echo "Calling {$url}<br>";

			echo "TestResponse:<br>" . SendCurlJsonRequest($url, $TestArray['method'], $this->model->testUpsertJson[$resource]['json']);
		}
		else 
			echo "Error: No such test defined.";
	}		
	
	function View($resource = '', $subresource = '') {
				
		if ($resource != '') {
			
			if ($resource == 'GET' || $resource == 'POST' || $resource == 'PUT' || $resource == 'DELETE') {						
				$resource = json_encode(GetActionRequest($resource));			
			}
			
			$this->model->SetEchoMessage($resource);
		}	
		
		$this->view->View($this->model->echoMessage);
				
		
	}

	
}
	
?>