<?php 

class PriceValidatorOrderModel extends AbstractModel {

	function __construct() {		
		parent::__construct();


		$this->testValidateJson['Test1'] = array('Description' => 'Validate Order', 'json' => '[{"PriceValidatorOrder":{"TransactionType":"1","CustomerIdentifier":"11100","CustomerOrderNumber":"","OrderLines":[{"ProductIdentifier":"1231","OrderQuantity":"2.000"},{"ProductIdentifier":"1233","OrderQuantity":"5.000"}]}}]');
		$this->testValidateJson['Test2'] = array('Description' => 'Validate Reversed Order', 'json' => '[{"PriceValidatorOrder":{"TransactionType":"91","CustomerIdentifier":"11100","CustomerOrderNumber":"","OrderLines":[{"ProductIdentifier":"1231","OrderQuantity":"-2.000","UnitPriceBase":"I","UnitPriceAmount":"24.145","UnitDiscountType":"P","UnitDiscountAmount":"0.000","UnitPromoType":"P","UnitPromoAmount":"0.000"},{"ProductIdentifier":"1233","OrderQuantity":"-5.000","UnitPriceBase":"I","UnitPriceAmount":"35.760","UnitDiscountType":"P","UnitDiscountAmount":"0.000","UnitPromoType":"P","UnitPromoAmount":"0.000"}]}}]');


	}

}

?>