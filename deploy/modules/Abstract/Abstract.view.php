<?php 


class AbstractView {
	protected $list;
	protected $custom_header;

	function __construct() {
		$this->custom_header = array("");
	}
	
	protected function CustomHeader() {
		return implode(PHP_EOL, $this->custom_header);
		
	}	
	
	function SetList($list) {
		$this->list = $list;
	}
	
	public function View($Object) {
		echo "abstract view";
	}
	
	public function ViewAll($Object) {
		echo "abstract view all";
	}
	
	public function Search($SearchParams, $SearchResults) {
		echo "";
	}
	
	public function Edit($Object) {
		echo "AbstractView->Edit();";
	}
	
}
	
?>