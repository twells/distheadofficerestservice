<?php 



class AbstractController {
	protected $model;
	protected $view;
	protected $maximumRequests = 10;
	protected static $ModuleName = '';
	
	function __construct() {		
		$view_class = self::$ModuleName . 'View';
		
		$this->view = new $view_class(); 
		
		$model_class = self::$ModuleName . 'Model';
		$this->model = new $model_class();
		

	}
	
	function Usage($resource = '', $subresource = '') {
		
		$UsageFile = "modules/" . self::$ModuleName ."/" . self::$ModuleName . ".usage.json";
		
		$main_content = "";
		
		SiteHeader();
		
		$Title = "Usage details for " . self::$ModuleName;
		if ($resource != '')
			$Title .= " - {$resource}";
		
		if ($subresource != 'NO_HEADER_FOOTER')		
			TopNavigation($Title);
		

		
		if (file_exists($UsageFile)) {
			$json_file_data = file_get_contents($UsageFile);
			$Actions = json_decode($json_file_data, true);
		
			if ($resource == '') {
				$main_content .= "<div class='alert-box secondary' style='font-size:26px;'>Available Services</div>";
				$main_content .= "<p style='font-size:18px;'><u>Click a service below to view full usage details</u></p>";
				$main_content .= "<table>";
				$main_content .= "<tr><th><a href='" . self::$ModuleName . "/Usage/All'>All</a></th><td>Click to view all usage details for " . self::$ModuleName . "</td></tr>";
				foreach ($Actions as $ActionUsage) {
					$main_content .= "<tr><th><a href='" . self::$ModuleName . "/Usage/{$ActionUsage['Action']}'>{$ActionUsage['Action']}</a></th><td>{$ActionUsage['Description']}</td></tr>";
				}
				$main_content .= "</table>";
			}
			else {
				foreach ($Actions as $ActionUsage) {
					if ($resource == '' || $resource == 'All' || $resource == $ActionUsage['Action'])
						$main_content .= $this->_usage($ActionUsage);
				}
					
			}	
		}
		else 
			$main_content .= "No usage details configured";	
		
		$main_content = str_replace(RESTSERVICESROOT_TAG, REST_SERVICE_HOME, $main_content);
		
		echo $main_content;
		
		
		if ($subresource != 'NO_HEADER_FOOTER')		
			SiteFooter();
	}
	
	function ValidateJsonRequestAgainstChecksum($request_checksum, $http_raw_data) {
		
		if (is_null($request_checksum) || trim($request_checksum) == '')
			OutputResponseAndExit(400, "Invalid Request. 'RequestChecksum' was not supplied");		
		
		$calculated_checksum = md5($http_raw_data);
		
		if ($request_checksum != $calculated_checksum)
			OutputResponseAndExit(400, "Invalid Request. The 'RequestChecksum' of {$request_checksum} does not match the supplied json {$calculated_checksum}.");	
		
	}
	
	function ValidationUpsertRequest($http_raw_data, &$request_array) {
		
		if (!MyDecodeJson($http_raw_data, $request_array, $error)) {
			OutputResponseAndExit(400, "Invalid Request. Json malformed - {$error}");			
		}
		
		if (!is_array($request_array)) {
			OutputResponseAndExit(400, "Invalid Request. An array of requests was expected.");
		}
		
		if ($this->maximumRequests > 0 && $this->maximumRequests < count($request_array)) {
			OutputResponseAndExit(431, "Invalid Request. Maximum number of requests exceeded, please refer to documentation.");
		}
		
		//foreach ($request_array as $request_item) {
		
		//}	
		
	}
	
	function ValidationDeleteRequest($http_raw_data, &$request_array) {
		if (!MyDecodeJson($http_raw_data, $request_array, $error)) {
			OutputResponseAndExit(400, "Invalid Request. Json malformed - {$error}");			
		}
		
		if (!is_array($request_array)) {
			OutputResponseAndExit(400, "Invalid Request. An array of requests was expected.");
		}
		
		if ($this->maximumRequests > 0 && $this->maximumRequests < count($request_array)) {
			OutputResponseAndExit(431, "Invalid Request. Maximum number of requests exceeded, please refer to documentation.");
		}		
		
	}
	

	function ValidationValidateRequest($http_raw_data, &$request_array) {
		
		if (!MyDecodeJson($http_raw_data, $request_array, $error)) {
			OutputResponseAndExit(400, "Invalid Request. Json malformed - {$error}");			
		}
		
		if (!is_array($request_array)) {
			OutputResponseAndExit(400, "Invalid Request. An array of requests was expected.");
		}
		
		if ($this->maximumRequests > 0 && $this->maximumRequests < count($request_array)) {
			OutputResponseAndExit(431, "Invalid Request. Maximum number of requests exceeded, please refer to documentation.");
		}
		
		//foreach ($request_array as $request_item) {
		
		//}	
		
	}		
	
	// $resource should be a json string contain data to upsert
	function Upsert($resource = null, $subresource = null) {		
		
		if ($this->ClUpsertCommand == "") {
			OutputResponseAndExit(501, "ClUpsertCommand is not defined on server.");	
		}
		
		$http_raw_data = GetActionRequest('PUT');
		
		$this->ValidateJsonRequestAgainstChecksum($resource, $http_raw_data); // if invalid it will return error to callee
		$this->ValidationUpsertRequest($http_raw_data, $request_array); // if invalid it will return error to callee
		
		$item_responses = array();
		
		// minimum of 60 seconds set and we will allow 5 seconds per cl request
		set_time_limit(60 + (count($request_array) * 5));
		
		$status = 200; // init to everything okay, if there is an error with a particular response, then set to 207
		
		foreach ($request_array as $request_item) {

			$cl_request = array();
			$cl_request['Table'] = self::$ModuleName;
			$cl_request['Action'] = __FUNCTION__;
			$cl_request['Key'] = $request_item['Key'];
			$cl_request[$cl_request['Table']] = $request_item[$cl_request['Table']];
						
			$json_request = json_encode($cl_request);
			
			if (!file_exists('/tmp/distHOrest')) { 
				$oldumask = umask(0); 
				mkdir('/tmp/distHOrest', 0777);
				umask($oldumask); 
			}
			
			
			$tmpFilePrefix = "/tmp/distHOrest/distHO_" . self::$ModuleName . "Upsert" . uniqid();
			$inputFile = $tmpFilePrefix . ".txt";
			$outputFile = $tmpFilePrefix . ".log";
			
				
			$command = str_replace(CLINPUTFILE_REPLACETAG, $inputFile, $this->ClUpsertCommand);
			
			//OutputResponseAndExit(200, $command);	// just for dev testing
			
			file_put_contents($inputFile, $json_request);
						
			$last_line = exec($command, $output, $return_var);

			unlink($inputFile);
			

			if (file_exists($outputFile)) {
				$responseJson = file_get_contents($outputFile);
				$cl_response = json_decode($responseJson, true);
				unlink($outputFile);
			}
			else {
				$cl_response = array('StatusCode' => 500, 'StatusText' => "No response detected. Error on server = {$last_line}");
			}

			
			if ($status == 200 && $cl_response['StatusCode'] != 200) 
				$status = 207;
			
			$item_response = GenerateResponse($cl_response['StatusCode'], $cl_response['StatusText'], array('Key' => $request_item['Key']));
			
			LogResponse($item_response);
			
			$item_responses[] = $item_response;
					
		}
		
		$Response = GenerateResponse($status, 'Requests complete, check responses for individual status.', null, $resource);
		$Response['Responses'] = $item_responses; 
		
		OutputResponseArrayAndExit($Response);
		
	}
	
	// $resource should be a json string contain data to Validate
	function Validate($resource = null, $subresource = null) {		
		
		if ($this->ClValidateCommand == "") {
			OutputResponseAndExit(501, "ClValidateCommand is not defined on server.");	
		}
		
		$http_raw_data = GetActionRequest('POST');
		
		$this->ValidateJsonRequestAgainstChecksum($resource, $http_raw_data); // if invalid it will return error to callee
		$this->ValidationValidateRequest($http_raw_data, $request_array); // if invalid it will return error to callee
		
		$item_responses = array();
		
		// minimum of 60 seconds set and we will allow 5 seconds per cl request
		set_time_limit(60 + (count($request_array) * 5));
		
		$status = 200; // init to everything okay, if there is an error with a particular response, then set to 207
		
		foreach ($request_array as $request_item) {

			$cl_request = array();
			$cl_request['Table'] = self::$ModuleName;
			$cl_request['Action'] = __FUNCTION__;
			//$cl_request['Key'] = $request_item['Key'];
			$cl_request[$cl_request['Table']] = $request_item[$cl_request['Table']];
						
			$json_request = json_encode($cl_request);
			
			if (!file_exists('/tmp/distHOrest')) { 
				$oldumask = umask(0); 
				mkdir('/tmp/distHOrest', 0777);
				umask($oldumask); 
			}
			
			
			$tmpFilePrefix = "/tmp/distHOrest/distHO_" . self::$ModuleName . "Validate" . uniqid();
			$inputFile = $tmpFilePrefix . ".txt";
			$outputFile = $tmpFilePrefix . ".log";
			
				
			$command = str_replace(CLINPUTFILE_REPLACETAG, $inputFile, $this->ClValidateCommand);
			
			//OutputResponseAndExit(200, $command);	// just for dev testing
			
			file_put_contents($inputFile, $json_request);
						
			$last_line = exec($command, $output, $return_var);

			unlink($inputFile);
			

			if (file_exists($outputFile)) {
				$responseJson = file_get_contents($outputFile);
				$cl_response = json_decode($responseJson, true);
				unlink($outputFile);
			}
			else {
				$cl_response = array('StatusCode' => 500, 'StatusText' => "No response detected. Error on server = {$last_line}");
			}

			
			if ($status == 200 && $cl_response['StatusCode'] != 200) 
				$status = 207;
			
			$item_response = GenerateResponse($cl_response['StatusCode'], $cl_response['StatusText'], $cl_response['Response']);
			
			LogResponse($item_response);
			
			$item_responses[] = $item_response;
					
		}
		
		$Response = GenerateResponse($status, 'Requests complete, check responses for individual status.', null, $resource);
		$Response['Responses'] = $item_responses; 
		
		OutputResponseArrayAndExit($Response);
		
	}	
	
	function Delete($resource, $subresource = null) {
		$http_raw_data = GetActionRequest('DELETE');
		
		$this->ValidateJsonRequestAgainstChecksum($resource, $http_raw_data); // if invalid it will return error to callee
		$this->ValidationDeleteRequest($http_raw_data, $request_array); // if invalid it will return error to callee
		
		$item_responses = array();
		
		// minimum of 60 seconds set and we will allow 5 seconds per cl request
		set_time_limit(60 + (count($request_array) * 5));		
		
		$status = 200; // init to everything okay, if there is an error with a particular response, then set to 207
		
		foreach ($request_array as $request_item) {

			$cl_request = array();
			$cl_request['Table'] = self::$ModuleName;
			$cl_request['Action'] = __FUNCTION__;
			$cl_request['Key'] = $request_item['Key'];
			
						
			$json_request = json_encode($cl_request);
			
			if (!file_exists('/tmp/distHOrest')) { 
				$oldumask = umask(0); 
				mkdir('/tmp/distHOrest', 0777);
				umask($oldumask); 
			}
			
			
			$tmpFilePrefix = "/tmp/distHOrest/distHO_" . self::$ModuleName . "Delete" . uniqid();
			$inputFile = $tmpFilePrefix . ".txt";
			$outputFile = $tmpFilePrefix . ".log";
			$command = str_replace(CLINPUTFILE_REPLACETAG, $inputFile, $this->ClDeleteCommand);
			
			file_put_contents($inputFile, $json_request);
						
			$last_line = exec($command, $output, $return_var);

			unlink($inputFile);
					
			if (file_exists($outputFile)) {
				$responseJson = file_get_contents($outputFile);
				$cl_response = json_decode($responseJson, true);
				unlink($outputFile);
			}
			else {
				$cl_response = array('StatusCode' => 500, 'StatusText' => "No response detected. Error on server = {$last_line}");
			}				
			
			if ($status == 200 && $cl_response['StatusCode'] != 200) 
				$status = 207;			
			
			$item_response = GenerateResponse($cl_response['StatusCode'], $cl_response['StatusText'], array('Key' => $request_item['Key']));
			
			LogResponse($item_response);
			
			$item_responses[] = $item_response;
					
		}
		
		$Response = GenerateResponse($status, 'Requests complete, check responses for individual status.', null, $resource);
		$Response['Responses'] = $item_responses; 
		
		OutputResponseArrayAndExit($Response);
	}
	
	
	// $resource should be a json string contain data to replace
	
	
	protected function _usage($ActionUsage) {
		//echo "<hr>";
		
		$content = "";
		
		$content .=  "<div class='alert-box secondary' style='page-break-before: always; font-size:26px;'>Module: " . self::$ModuleName . "<br>Action: {$ActionUsage['Action']}</div>";
		
		$content .=  "<p style='font-size:18px;'><u>Overview</u></p>";
		
		$content .=  "<table>";
		$content .=  "<tr><td><b>Description</b></td><td>{$ActionUsage['Description']}</td></tr>";
		
		$UrlStructure = str_replace('your.hostname', GetSiteHost(), $ActionUsage['UrlStructure']);
		$content .=  "<tr><td><b>Url Structure</b></td><td>{$UrlStructure}</td></tr>";
		
		if (isset($ActionUsage['UrlParameters']['Resource']))
			$content .=  "<tr><td><b>Resource</b></td><td>{$ActionUsage['UrlParameters']['Resource']}</td></tr>";
		if (isset($ActionUsage['UrlParameters']['SubResource']))
			$content .=  "<tr><td><b>Sub-Resource</b></td><td>{$ActionUsage['UrlParameters']['SubResource']}</td></tr>";
		
		if (isset($ActionUsage['UrlParameters']['Entity']))
			$content .=  "<tr><td><b>Entity</b></td><td>{$ActionUsage['UrlParameters']['Entity']}</td></tr>";
		if (isset($ActionUsage['UrlParameters']['Action']))
			$content .=  "<tr><td><b>Action</b></td><td>{$ActionUsage['UrlParameters']['Action']}</td></tr>";
		if (isset($ActionUsage['UrlParameters']['RequestId']))
			$content .=  "<tr><td><b>RequestId</b></td><td>{$ActionUsage['UrlParameters']['RequestId']}</td></tr>";
		else if (isset($ActionUsage['UrlParameters']['RequestChecksum']))
			$content .=  "<tr><td><b>RequestChecksum</b></td><td>{$ActionUsage['UrlParameters']['RequestChecksum']}</td></tr>";
		
		
		if (isset($ActionUsage['RequestMethod']))
			$content .=  "<tr><td><b>Request Method</b></td><td>{$ActionUsage['RequestMethod']}</td></tr>";
		
		if (isset($ActionUsage['ApplicationType']))
			$content .=  "<tr><td><b>Application Type</b></td><td>{$ActionUsage['ApplicationType']}</td></tr>";
		
		if (isset($ActionUsage['InputType']))
			$content .=  "<tr><td><b>Expected Input Format</b></td><td>{$ActionUsage['InputType']}</td></tr>";
		if (isset($ActionUsage['InputExpected']))
			$content .=  "<tr><td><b>Expected Input</b></td><td>{$ActionUsage['InputExpected']}</td></tr>";
		if (isset($ActionUsage['MaxRequests']))
			$content .=  "<tr><td><b>Maximum Requests</b></td><td>{$ActionUsage['MaxRequests']}</td></tr>";		
		
		if (isset($ActionUsage['OutputType']))
			$content .=  "<tr><td><b>Expected Input Format</b></td><td>{$ActionUsage['OutputType']}</td></tr>";
		if (isset($ActionUsage['OutputExpected']))
			$content .=  "<tr><td><b>Expected Input</b></td><td>{$ActionUsage['OutputExpected']}</td></tr>";		

		$content .=  "</table>";
		
		
		if (isset($ActionUsage['AvailableFields'])) {
			
			$content .=  "<p style='font-size:18px;'><u>Available Fields</u></p>";
			
			$content .=  "<table><tbody>";
			foreach ($ActionUsage['AvailableFields'] as $key => $value)
				$content .=  "<tr><td><b>{$key}</b></td><td>{$value}</td></tr>";
			$content .=  "</tbody></table>";
		}		
		
		if (isset($ActionUsage['Examples'])) {
			
			$content .=  "<p style='font-size:18px;'><u>Example Requests</u></p>";
			
			foreach ($ActionUsage['Examples'] as $Example) {
				if (isset($Example['ExampleUrl'])) {
					$ExampleUrl = str_replace('your.hostname', GetSiteHost(), $Example['ExampleUrl']);
					$content .=  "<p>{$ExampleUrl}</p>";
				}
				
				if (isset($Example['ExampleFields'])) {
					$content .=  "<pre style='font-size: 12px; padding:5px; width:750px; border: 1px solid lightgrey; border-left: 12px solid green; border-radius: 5px; padding: 14px;'>" . prettyPrint(json_encode($Example['ExampleFields'])) . "</pre><br>";
				}
			}
		}
		else {
			if (isset($ActionUsage['ExampleUrl'])) {
				
				$content .=  "<p style='font-size:18px;'><u>Example Request</u></p>";
				
				$ExampleUrl = str_replace('your.hostname', GetSiteHost(), $ActionUsage['ExampleUrl']);
				$content .=  "<p>{$ExampleUrl}</p>";
			}
			
			if (isset($ActionUsage['ExampleFields'])) {
				$content .=  "<pre style='font-size: 12px; padding:5px; width:750px; border: 1px solid lightgrey; border-left: 12px solid green; border-radius: 5px; padding: 14px;'>" . prettyPrint(json_encode($ActionUsage['ExampleFields'])) . "</pre><br>";
			}
		}
		
		if (isset($ActionUsage['ExampleUrl'])) {
			
			$content .=  "<p style='font-size:18px;'><u>Example Response</u></p>";
			
			$ExampleUrl = str_replace('your.hostname', GetSiteHost(), $ActionUsage['ExampleUrl']);
			$content .=  "<p>{$ExampleUrl}</p>";
		}
		
		if (isset($ActionUsage['ExampleResponse'])) {
			$content .=  "<p><i>N.b - The value in the 'Status' code will also be set as the HTTP response code</i></p>";
			$content .=  "<pre style='font-size: 12px; padding:5px; width:750px; border: 1px solid lightgrey; border-left: 12px solid green; border-radius: 5px; padding: 14px;'>" . prettyPrint(json_encode($ActionUsage['ExampleResponse'])) . "</pre><br>";
			
		}		
		
		if (isset($ActionUsage['MainResponseCodes']) || isset($ActionUsage['ResponseArrayCodes'])) {
			$content .=  "<p style='font-size:18px;'><u>Return Codes</u></p>";
						
			if (isset($ActionUsage['MainResponseCodes'])) {
				$content .=  "<p>The below codes refer to the list of possible codes that could be returned from the request:</p>";
				
				$content .=  "<table><tbody>";
				$content .=  "<tr><th>Code</th><th>&nbsp;</th></tr>";
				foreach ($ActionUsage['MainResponseCodes'] as $ResponseCode) {						
					$content .=  "<tr><td><b>{$ResponseCode['Status']}</b></td><td>{$ResponseCode['Description']}</td></tr>";											
				}
				$content .=  "</tbody></table>";
			}
			
			
			if (isset($ActionUsage['ResponseArrayCodes'])) {
				$content .=  "<p>The below codes refer to the list of possible codes that could be returned for each response object returned in the 'Responses' array supplied:</p>";
				
				$content .=  "<table><tbody>";
				$content .=  "<tr><th>Code</th><th>&nbsp;</th></tr>";
				foreach ($ActionUsage['ResponseArrayCodes'] as $ResponseCode) {						
					$content .=  "<tr><td><b>{$ResponseCode['Status']}</b></td><td>{$ResponseCode['Description']}</td></tr>";											
				}
				$content .=  "</tbody></table>";
				
				$content .=  "<p><i>The callee should refer to specific messages returned for specific detail on a response.</i></p>";
			}
		}
		
		if (isset($ActionUsage['ExpandedUsage'])) {
			
			$content .=  "<p style='font-size:18px;'><u>Expanded Usage</u></p>";
			
			$content .=  "<table><thead><tr><th>Field</th><th>Valid Values</th><th>On Insert</th><th>On Update</th></tr></thead><tbody>";
			foreach ($ActionUsage['ExpandedUsage'] as $field => $ExpandedUsage) {
				if ($field == 'Example')
					continue;
				$content .=  "<tr><td><b>{$field}</b></td><td>{$ExpandedUsage['ValidValues']}</td><td>{$ExpandedUsage['OnInsert']}</td><td>{$ExpandedUsage['OnUpdate']}</td></tr>";
			}
			$content .=  "</tbody></table>";			
		}
		
		$content .= "<hr>";
		
		return $content;
	}

	function UpsertTest($resource) {
		echo "Test Id: {$resource}<br>";
		if (isset($this->model->testUpsertJson[$resource])) {
			echo "Test Description: {$this->model->testUpsertJson[$resource]['Description']}<br>";			
			$json = $this->model->testUpsertJson[$resource]['json'];			
			$RequestId = md5($json);	
			$url = DISTHO_HOST . self::$ModuleName . "/Upsert/" . $RequestId;
			echo "TestUrl:  {$url}<br>";
			echo "TestRequest:<br>{$json}<br><br>";
			echo "TestResponse:<br>" . SendCurlJsonRequest($url, "PUT", $json);
			
		}
		else 
			echo "Error: No such test defined.";
	}
	
	function DeleteTest($resource) {		
		echo "Test Id: {$resource}<br>";
		if (isset($this->model->testDeleteJson[$resource])) {
			echo "Test Description: {$this->model->testDeleteJson[$resource]['Description']}<br>";			
			$json = $this->model->testDeleteJson[$resource]['json'];			
			$RequestId = md5($json);	
			$url = DISTHO_HOST . self::$ModuleName . "/Delete/" . $RequestId;
			echo "TestUrl:  {$url}<br>";
			echo "TestRequest:<br>{$json}<br><br>";
			echo "TestResponse:<br>" . SendCurlJsonRequest($url, "DELETE", $json);
			
		}
		else 
			echo "Error: No such test defined.";
	}
	
	function ValidateTest($resource) {
		echo "Test Id: {$resource}<br>";
		if (isset($this->model->testValidateJson[$resource])) {
			echo "Test Description: {$this->model->testValidateJson[$resource]['Description']}<br>";			
			$json = $this->model->testValidateJson[$resource]['json'];			
			$RequestId = md5($json);	
			$url = DISTHO_HOST . self::$ModuleName . "/Validate/" . $RequestId;
			echo "TestUrl:  {$url}<br>";
			echo "TestRequest:<br>{$json}<br><br>";
			echo "TestResponse:<br>" . SendCurlJsonRequest($url, "POST", $json);
			
		}
		else 
			echo "Error: No such test defined.";
	}		

}

?>