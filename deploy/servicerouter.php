<?php 

	require_once('inc/utils/site_lib.php');
	require_once('modules/Abstract/Abstract.controller.php');
	require_once('modules/Abstract/Abstract.model.php');
	require_once('modules/Abstract/Abstract.view.php');
			
	$module = $_GET['module'];
	$action = $_GET['action'];
	$resource = isset($_GET['resource']) ? $_GET['resource'] : null;
	$subresource = isset($_GET['subresource']) ? $_GET['subresource'] : null;

	$target_dir = "modules/{$module}/";

	if (!file_exists($target_dir)) 
		OutputResponseAndExit(404, "The requested module '{$module}' does not exist.");

	
	foreach(glob("{$target_dir}*") as $filename) {

		if (file_exists($filename) && pathinfo($filename, PATHINFO_EXTENSION) == "php")
			require_once $filename;
	}

	$ControllerClass = $module . "Controller";
	
	if (!class_exists($ControllerClass))
		OutputResponseAndExit(500, "The requested controller '{$ControllerClass}' does not exist.");
	
	$controller = new $ControllerClass();

	if (method_exists($controller,$action)) {
		if ($resource && $subresource) 
			$controller->$action($resource, $subresource);
		else if ($resource) 
			$controller->$action($resource);
		else 	
			$controller->$action();
	}
	else 
		OutputResponseAndExit(404, "The requested webservice '{$action}' does not exist.");

?>